<?php
declare(strict_types=1);

namespace Vortice\Utils\Wrappers;

use JetBrains\PhpStorm\Pure;

class StringWrapper
{
    public function __construct(
        protected string $container,
        protected bool $changed = false,
        protected int $length = 0
    ) {
        $this->length = mb_strlen($this->container);
    }

    public static function create(string $container): static
    {
        return new static($container);
    }

    public function contains(string $needle): bool
    {
        return str_contains($this->container, $needle);
    }

    public function starts(string $with): bool
    {
        return str_starts_with($this->container, $with);
    }

    public function ends(string $with): bool
    {
        return str_ends_with($this->container, $with);
    }

    /**
     * Return an ArrayWrapper with the exploded content of the current string
     */
    #[Pure]
    public function explode(string $separator): ArrayWrapper
    {
        if (!empty($separator)) {
            return new ArrayWrapper(
                explode($separator, $this->container)
            );
        } else {
            return new ArrayWrapper([]);
        }
    }

    /**
     * Return an ArrayNumeric with the exploded content of the current string
     */
    #[Pure]
    public function explodeNumeric(string $separator): ArrayNumeric
    {
        if (!empty($separator)) {
            return new ArrayNumeric(
                explode($separator, $this->container)
            );
        } else {
            return new ArrayNumeric([]);
        }
    }

    public function length(): int
    {
        if ($this->changed) {
            $this->length = mb_strlen($this->container);
        }

        return $this->length;
    }
}