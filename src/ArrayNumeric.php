<?php

namespace Vortice\Utils\Wrappers;

/**
 * ArrayWrapper with numeric functions
 */
class ArrayNumeric extends ArrayWrapper
{

    /**
     * Try to reduce the array creating a sum of the elements
     */
    public function sum(): int
    {
        if ($this->changed || (null === $this->summarized)) {
            $this->summarized = array_reduce($this->collection, fn($carry, $item) => $carry += $item);
        }

        return $this->summarized;
    }
}