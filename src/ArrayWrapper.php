<?php
declare(strict_types=1);

namespace Vortice\Utils\Wrappers;

class ArrayWrapper
{
    public function __construct(
        protected array $collection,
        protected bool $changed = false,
        protected ?int $summarized = null,
    ) {

    }

    public static function create(array $collection): static
    {
        return new static($collection);
    }

    /**
     * Wrapper method for array_filter
     */
    public function filter(callable $closure): self
    {
        $this->collection = array_filter($this->collection, $closure);
        $this->changed = true;
        return $this;
    }

    /**
     * Wrapper method for array_map
     */
    public function map(callable $closure): self
    {
        $this->collection = array_map($closure, $this->collection);
        $this->changed = true;
        return $this;
    }

    public function reduce(callable $closure): mixed
    {
        return array_reduce($this->collection, $closure);
    }

    public function splice(int $offset, ?int $length = null, array $replacement = []): self
    {
        array_splice($this->collection, $offset, $length, $replacement);
        $this->changed = true;
        return $this;
    }

    public function deswrap(): array {
        return $this->collection;
    }
}