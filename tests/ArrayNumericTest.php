<?php

namespace Vortice\Utils\Wrapers\Tests;

use Vortice\Utils\Wrappers\ArrayNumeric;
use PHPUnit\Framework\TestCase;

class ArrayNumericTest extends TestCase
{
    public function testVariousSum1()
    {
        $arrayWrapper = ArrayNumeric::create([1, 2, 3, 4]);
        $this->assertEquals(18, $arrayWrapper
            ->map(fn($iteration) => $iteration * 2)
            ->splice(1, 1, [2])
            ->sum()
        );
        $this->assertEquals(18, $arrayWrapper->sum());
    }
}
