<?php

namespace Vortice\Utils\Wrapers\Tests;

use Vortice\Utils\Wrappers\ArrayWrapper;
use PHPUnit\Framework\TestCase;

class ArrayWrapperTest extends TestCase
{

    public function testReduce()
    {
        $this->assertEquals(15, ArrayWrapper::create([1, 2, 3, 4, 5])
            ->reduce(function($carry, $item) {
                $carry += $item;
                return $carry;
            }));
    }

    public function testFilter()
    {
        $this->assertEqualsCanonicalizing([1, 3, 5], ArrayWrapper::create([1, 2, 3, 4, 5])
            ->filter(fn($iteration) => ($iteration & 1))->deswrap());
    }

    public function testMap()
    {
        $arrayWrapper = new ArrayWrapper([1, 2, 3, 4]);
        $this->assertEquals([2, 4, 6, 8], $arrayWrapper
            ->map(fn($iteration) => $iteration * 2 )->deswrap());
    }

    public function testVarious()
    {
        $arrayWrapper = ArrayWrapper::create([1, 2, 3, 4]);
        $this->assertEquals(18, $arrayWrapper
            ->map(fn($iteration) => $iteration * 2)
            ->splice(1, 1, [2])
            ->reduce(function($carry, $item) {
                $carry += $item;
                return $carry;
            })
        );
    }
}
