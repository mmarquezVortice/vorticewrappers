<?php

namespace Vortice\Utils\Wrapers\Tests;

use Vortice\Utils\Wrappers\ArrayNumeric;
use Vortice\Utils\Wrappers\StringWrapper;
use PHPUnit\Framework\TestCase;

class StringWrapperTest extends TestCase
{

    public function testExplode()
    {
        $testText = StringWrapper::create('1;2;3;4;5;6;7;8;9');
        $this->assertEquals(45,
            $testText->explodeNumeric(';')
                ->sum()
        );
    }

    public function testContains()
    {
        $testText = StringWrapper::create('Probando texto');
        $this->assertTrue($testText->contains('bando'));
    }

    public function testStarts()
    {
        $testText = StringWrapper::create('Probando texto');
        $this->assertTrue($testText->starts('Pro'));
    }

    public function testEnds()
    {
        $testText = StringWrapper::create('Probando texto');
        $this->assertTrue($testText->ends('texto'));
    }

    public function testLength()
    {
        $testText = StringWrapper::create('Probando texto');
        $this->assertEquals(14, $testText->length());

        $testText = StringWrapper::create('测试文本');
        $this->assertEquals(4, $testText->length());
    }
}
